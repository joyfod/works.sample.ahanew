﻿using Component.Base;
using Component.Helper;
using ECSystem.API.Filter;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Service.Implement;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace ECSystem.API.Controllers
{
    public class MemberShipController : ApiController
    {
        [EnableCors(origins: "*", headers: "*", methods: "*", SupportsCredentials = true)]
        [HttpPost]
        [AllowAnonymous]
        public HttpResponseMessage Login([FromBody] JObject jo)
        {
            if (jo is null)
            {
                throw new ArgumentNullException(nameof(jo));
            }

            Hashtable hashtable = new Hashtable();
            string accessLogID = "";

            // 設定傳入參數
            string memberUser = string.Empty;                   // 登入帳號
            string pwd = string.Empty;                          // 登入密碼
            string plateForm = string.Empty;                    // 登入平台
            string browerInfo = string.Empty;
            string inputCode = string.Empty;
            string validCode = string.Empty;
            string googleAuth = string.Empty;
            string fbAuth = string.Empty;
            bool isMandatory = false;

            try
            {
                var request = HttpContext.Current.Request;
                string ip = UserIPHelper.GetUserIP();
                string userAgent = string.Empty;
                //string userAgent = request.Headers.GetValues("User-Agent").FirstOrDefault();
                try
                {
                    userAgent = request.Headers.GetValues("UserAgent").FirstOrDefault();
                    if (!string.IsNullOrEmpty(userAgent))
                    {
                        plateForm = userAgent.Split('#').FirstOrDefault();
                        browerInfo = userAgent.Split('#')[1];
                    }
                }
                catch (Exception er)
                {
                    userAgent = "No user agent!!!";
                    plateForm = "No user plateForm!!!";
                    browerInfo = "No user browerInfo!!!";
                }

                bool testBool = false;
                #region 前端傳入資料轉換及驗證
                memberUser = jo.Property("UserName") != null ? Convert.ToString(jo["UserName"]) : memberUser;
                pwd = jo.Property("Pwd") != null ? Convert.ToString(jo["Pwd"]) : pwd;
                inputCode = jo.Property("InputCode") != null ? Convert.ToString(jo["InputCode"]) : inputCode;
                validCode = jo.Property("ValidatorCode") != null ? Convert.ToString(jo["ValidatorCode"]) : validCode;
                isMandatory = bool.TryParse(Convert.ToString(jo["IsMandatory"]), out testBool) ? testBool : isMandatory;
                googleAuth = jo.Property("GoogleAuth") != null ? Convert.ToString(jo["GoogleAuth"]) : googleAuth;
                fbAuth = jo.Property("FaceBookAuth") != null ? Convert.ToString(jo["FaceBookAuth"]) : fbAuth;
                #endregion

                MemberShipService memberShipService = new MemberShipService();
                hashtable = memberShipService.Login(memberUser, pwd, ip, plateForm, browerInfo, userAgent, validCode, inputCode, isMandatory);
            }
            catch (Exception er)
            {
                APIResObj apiResObj = new APIResObj();
                apiResObj.ResCode = Convert.ToInt32(HttpStatusCode.BadRequest).ToString();
                apiResObj.ResMessage = er.Message;
                hashtable.Add("APIRes", apiResObj);
                return Request.CreateResponse(HttpStatusCode.OK, hashtable);

                //hashtable.Add("ErMsg", er.Message);
                //HttpError err = new HttpError();
                //err["ErMsg"] = er.Message;
                //return Request.CreateErrorResponse(HttpStatusCode.BadRequest, err);
            }
            finally
            {
                //LogHelper.AccessEnd(this.GetType(), accessLogID, JsonConvert.SerializeObject(hashtable));
            }

            return Request.CreateResponse(HttpStatusCode.OK, hashtable);
        }


        [EnableCors(origins: "*", headers: "*", methods: "*", SupportsCredentials = true)]
        [HttpPost]
        [CustomerFilter(FunctionName = "Logout")]
        public HttpResponseMessage Logout([FromBody] JObject jo)
        {
            Hashtable hashtable = new Hashtable();
            string accessLogID = "";

            try
            {
                string userToken = UserTokenHelper.GetUserToken(HttpContext.Current);

                #region 前端傳入資料轉換及驗證
                try
                {
                    MemberShipService memberShipService = new MemberShipService();
                    hashtable = memberShipService.Logout(userToken);
                }
                catch (Exception er)
                {
                    hashtable.Add("ErMsg", er.Message);
                    HttpError err = new HttpError();
                    err["ErMsg"] = er.Message;
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, err);
                }
                #endregion

            }
            catch (Exception ex)
            {
                hashtable.Add("ExMsg", ex.Message);
                HttpError err = new HttpError();
                err["ExMsg"] = ex.Message;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, err);
            }
            finally
            {
                //LogHelper.AccessEnd(this.GetType(), accessLogID, JsonConvert.SerializeObject(hashtable));
            }

            return Request.CreateResponse(HttpStatusCode.OK, hashtable);
        }

        // GET: /Account/ConfirmEmail
        [EnableCors(origins: "*", headers: "*", methods: "*", SupportsCredentials = true)]
        [AllowAnonymous]
        [HttpGet]
        public HttpResponseMessage ConfirmEmail(string publicKey, string code)
        {
            Hashtable hashtable = new Hashtable();

            if (publicKey == null || code == null)
            {
                hashtable.Add("ExMsg", "Confirm Error!!");
                HttpError err = new HttpError();
                err["ExMsg"] = "Confirm Error!!";
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, err);
            }

            MemberShipService memberShipService = new MemberShipService();
            hashtable = memberShipService.ConfirmEmail(publicKey, code);
            return Request.CreateResponse(HttpStatusCode.OK, hashtable);
        }
    }
}