﻿using Component.Base;
using Component.Helper;
using ECSystem.API.Filter;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Service.Implement;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace ECSystem.API.Controllers
{
    public class CommodityController : ApiController
    {
        [EnableCors(origins: "*", headers: "*", methods: "*", SupportsCredentials = true)]
        [HttpPost]
        public HttpResponseMessage InsterCommodity([FromBody] JObject jo)
        {
            if (jo is null)
            {
                throw new ArgumentNullException(nameof(jo));
            }

            Hashtable hashtable = new Hashtable();
            string accessLogID = "";

            // 設定傳入參數
            string productName = string.Empty;                  // 登入帳號
            int qty = 1000;                                     // 預存數量
            decimal price = 0M;                                 // 價格
            decimal inventory = 0M;                             // 存貨量

            try
            {
                bool testBool = false;
                int testInt = 0;
                decimal testDecimal = 0M;
                #region 前端傳入資料轉換及驗證
                productName = jo.Property("ProductName") != null ? Convert.ToString(jo["ProductName"]) : productName;
                qty = int.TryParse(Convert.ToString(jo["Qty"]), out testInt) ? testInt : qty;
                price = decimal.TryParse(Convert.ToString(jo["Price"]), out testDecimal) ? testDecimal : price;
                inventory = decimal.TryParse(Convert.ToString(jo["Inventory"]), out testDecimal) ? testDecimal : inventory;
                #endregion

                CommodityService commodityService = new CommodityService();
                hashtable = commodityService.InsterCommodity(productName, qty, price, inventory);
            }
            catch (Exception er)
            {
                APIResObj apiResObj = new APIResObj();
                apiResObj.ResCode = Convert.ToInt32(HttpStatusCode.BadRequest).ToString();
                apiResObj.ResMessage = er.Message;
                hashtable.Add("APIRes", apiResObj);
                return Request.CreateResponse(HttpStatusCode.OK, hashtable);

                //hashtable.Add("ErMsg", er.Message);
                //HttpError err = new HttpError();
                //err["ErMsg"] = er.Message;
                //return Request.CreateErrorResponse(HttpStatusCode.BadRequest, err);
            }
            finally
            {
                //LogHelper.AccessEnd(this.GetType(), accessLogID, JsonConvert.SerializeObject(hashtable));
            }

            return Request.CreateResponse(HttpStatusCode.OK, hashtable);
        }

        [EnableCors(origins: "*", headers: "*", methods: "*", SupportsCredentials = true)]
        [HttpPost]
        public HttpResponseMessage FindCommodity([FromBody] JObject jo)
        {
            Hashtable hashtable = new Hashtable();
            string accessLogID = "";

            // 設定傳入參數
            string productName = string.Empty;                  // 登入帳號
            int qty = 1000;                                     // 預存數量
            decimal price = 0M;                                 // 價格
            decimal inventory = 0M;                             // 存貨量
            try
            {
                bool testBool = false;
                int testInt = 0;
                decimal testDecimal = 0M;
                #region 前端傳入資料轉換及驗證
                productName = jo.Property("ProductName") != null ? Convert.ToString(jo["ProductName"]) : productName;
                qty = int.TryParse(Convert.ToString(jo["Qty"]), out testInt) ? testInt : qty;
                price = decimal.TryParse(Convert.ToString(jo["Price"]), out testDecimal) ? testDecimal : price;
                inventory = decimal.TryParse(Convert.ToString(jo["Inventory"]), out testDecimal) ? testDecimal : inventory;
                #endregion

                CommodityService commodityService = new CommodityService();
                hashtable = commodityService.FindCommodity(productName, qty, price, inventory);

            }
            catch (Exception ex)
            {
                hashtable.Add("ExMsg", ex.Message);
                HttpError err = new HttpError();
                err["ExMsg"] = ex.Message;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, err);
            }
            finally
            {
                //LogHelper.AccessEnd(this.GetType(), accessLogID, JsonConvert.SerializeObject(hashtable));
            }

            return Request.CreateResponse(HttpStatusCode.OK, hashtable);
        }

        [EnableCors(origins: "*", headers: "*", methods: "*", SupportsCredentials = true)]
        [HttpPost]
        public HttpResponseMessage UpdateCommodity([FromBody] JObject jo)
        {
            if (jo is null)
            {
                throw new ArgumentNullException(nameof(jo));
            }

            Hashtable hashtable = new Hashtable();
            string accessLogID = "";

            // 設定傳入參數
            Guid productId = new Guid();                        // GUID
            string productName = string.Empty;                  // 登入帳號
            int qty = 1000;                                     // 預存數量
            decimal price = 0M;                                 // 價格
            decimal inventory = 0M;                             // 存貨量
            try
            {
                bool testBool = false;
                int testInt = 0;
                decimal testDecimal = 0M;
                #region 前端傳入資料轉換及驗證
                productId = jo.Property("ProductID") != null ? (Guid)(jo["ProductID"]) : productId;
                productName = jo.Property("ProductName") != null ? Convert.ToString(jo["ProductName"]) : productName;
                qty = int.TryParse(Convert.ToString(jo["Qty"]), out testInt) ? testInt : qty;
                price = decimal.TryParse(Convert.ToString(jo["Price"]), out testDecimal) ? testDecimal : price;
                inventory = decimal.TryParse(Convert.ToString(jo["Inventory"]), out testDecimal) ? testDecimal : inventory;
                #endregion

                CommodityService commodityService = new CommodityService();
                hashtable = commodityService.UpdateCommodity(productId,productName, qty, price, inventory);

            }
            catch (Exception ex)
            {
                hashtable.Add("ExMsg", ex.Message);
                HttpError err = new HttpError();
                err["ExMsg"] = ex.Message;
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, err);
            }
            finally
            {
                //LogHelper.AccessEnd(this.GetType(), accessLogID, JsonConvert.SerializeObject(hashtable));
            }

            return Request.CreateResponse(HttpStatusCode.OK, hashtable);
        }
    }
}