﻿using Component.Helper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace ECSystem.API.Filter
{
    public static class UserTokenHelper
    {
        private static RedisHelper redisHelper;

        public static string GetTokenByString(string key, string redisDB = "0", int lifeTimeMin = 3)
        {
            int testInt = 0;
            lifeTimeMin = int.TryParse(ConfigurationManager.AppSettings["PlayTokenOTP"], out testInt) ? testInt : lifeTimeMin;

            redisHelper = new RedisHelper(sDB: redisDB, syncTimeoutSec: lifeTimeMin);

            var ticket = redisHelper.GetCache(key);
            string userToken = ticket != null ? ticket.ToString() : null;
            return userToken;
        }

        public static string GetUserToken(HttpContext context, string redisDB = "0", int lifeTimeMin = 3)
        {
            string cacheKey = "";
            try
            {
                int testInt = 0;
                lifeTimeMin = int.TryParse(ConfigurationManager.AppSettings["UserTokenOTP"], out testInt) ? testInt : lifeTimeMin;

                redisHelper = new RedisHelper(sDB: redisDB, syncTimeoutSec: lifeTimeMin);

                var request = context.Request;
                cacheKey = request.Headers.GetValues("UserName")?.FirstOrDefault();
            }
            catch (Exception er)
            {

            }

            var ticket = cacheKey != null ? redisHelper.GetCache(cacheKey) : null;
            string userToken = ticket != null ? ticket.ToString() : null;
            return userToken;
        }

        public static string GetUserPlayToken(HttpContext context, string redisDB = "1", int lifeTimeMin = 3)
        {
            int testInt = 0;
            lifeTimeMin = int.TryParse(ConfigurationManager.AppSettings["PlayTokenOTP"], out testInt) ? testInt : lifeTimeMin;

            redisHelper = new RedisHelper(sDB: redisDB, syncTimeoutSec: lifeTimeMin);

            var request = context.Request;
            string cacheKey = request.Headers.GetValues("PlayToken")?.FirstOrDefault();
            var ticket = cacheKey != null ? redisHelper.GetCache(cacheKey) : null;
            string userToken = ticket != null ? ticket.ToString() : null;
            return userToken;
        }

        public static string GetUserRegisterToken(HttpContext context, string redisDB = "4", int lifeTimeMin = 3)
        {
            int testInt = 0;
            lifeTimeMin = int.TryParse(ConfigurationManager.AppSettings["RegisterOTP"], out testInt) ? testInt : lifeTimeMin;

            redisHelper = new RedisHelper(sDB: redisDB, syncTimeoutSec: lifeTimeMin);

            var request = context.Request;
            string cacheKey = request.Headers.GetValues("RegisterToken")?.FirstOrDefault();
            var ticket = cacheKey != null ? redisHelper.GetCache(cacheKey) : null;
            string userToken = ticket != null ? ticket.ToString() : null;
            return userToken;
        }

        public static string GetUserDepositToken(HttpContext context, string redisDB = "2", int lifeTimeMin = 3)
        {
            int testInt = 0;
            lifeTimeMin = int.TryParse(ConfigurationManager.AppSettings["PlayTokenOTP"], out testInt) ? testInt : lifeTimeMin;

            redisHelper = new RedisHelper(sDB: redisDB, syncTimeoutSec: lifeTimeMin);

            var request = context.Request;
            string cacheKey = request.Headers.GetValues("DepositToken")?.FirstOrDefault();
            var ticket = cacheKey != null ? redisHelper.GetCache(cacheKey) : null;
            string userToken = ticket != null ? ticket.ToString() : null;
            return userToken;
        }

        public static string GetUserWithDrawalToken(HttpContext context, string redisDB = "2", int lifeTimeMin = 3)
        {
            int testInt = 0;
            lifeTimeMin = int.TryParse(ConfigurationManager.AppSettings["PlayTokenOTP"], out testInt) ? testInt : lifeTimeMin;

            redisHelper = new RedisHelper(sDB: redisDB, syncTimeoutSec: lifeTimeMin);

            var request = context.Request;
            string cacheKey = request.Headers.GetValues("WithDrawalToken")?.FirstOrDefault();
            var ticket = cacheKey != null ? redisHelper.GetCache(cacheKey) : null;
            string userToken = ticket != null ? ticket.ToString() : null;
            return userToken;
        }
    }
}