﻿using Component.Base;
using Component.Helper;
using Component.Model;
using Newtonsoft.Json;
using Service.Implement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace ECSystem.API.Filter
{
    public class CustomerFilter : ActionFilterAttribute
    {
        public string FunctionName { get; set; } = null;
        public string GroupType { get; set; } = null;
        public string userToken { get; set; }
        public string thisData { get; set; } = "";
        private RedisHelper redisHelper = new RedisHelper();

        /// <summary>
        /// 驗證使用者權限，目前只有做到驗證登入，還未驗證權限，註記一下要補上驗證權限
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnActionExecuting(HttpActionContext filterContext)
        {
            APIResObj apiResObj = new APIResObj();
            apiResObj.FnName = $"{filterContext.ActionDescriptor.ActionName}";

            try
            {
                var request = HttpContext.Current.Request;

                // 取得使用者的Token
                string cacheKey = request.Headers.GetValues("UserName").FirstOrDefault();
                var ticket = redisHelper.GetCache(cacheKey);

                // 只要有人進入系統皆需記錄其操作行為LogHelp.WriteInfoLog($"{FunctionName}::{userToken}");
                userToken = ticket != null ? ticket.ToString() : null;

                if (!string.IsNullOrEmpty(userToken))
                {
                    MyWebSecurity webSecurity = new MyWebSecurity();
                    apiResObj = webSecurity.ChkUserToken(userToken);

                    string token = "";
                    MemberShip tokenMember = new MemberShip();
                    if (apiResObj.ResCode == "000")
                    {
                        //token = SecurityKeyHelper.Decrypt(userToken, true);
                        //tokenMember = JsonConvert.DeserializeObject<MemberShip>(token);

                        //MemberShipService memberShipService = new MemberShipService();
                        //MemberShip member = memberShipService.GetMemberByFilter(token, tokenMember.Account, false);

                        //DateTime today = DateTime.Now;
                    }
                }
                else
                {
                    apiResObj.ResCode = "200";
                    apiResObj.ResMessage = "Please Login！";

                    //{"APIRes":{"ResCode":"100","ResMsg":"登入人員員工識別碼不得為空"}} 
                    HttpResponseMessage httpResponseMessage = new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.OK,
                        Content = new StringContent("{\"APIRes\":" + JsonConvert.SerializeObject(apiResObj) + "}")
                    };

                    filterContext.Response = httpResponseMessage;
                    filterContext.Response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                }
            }
            catch (Exception er)
            {
                //LogHelp.WriteErrorLog($"OnActionExecuting: Error: {this.GetType().Name.ToString()}__{userToken}", er);
                HttpResponseMessage httpResponseMessage = new HttpResponseMessage()
                {
                    StatusCode = HttpStatusCode.InternalServerError,
                };

                filterContext.Response = httpResponseMessage;
            }
            finally
            {
                if (apiResObj.ResCode != "000")
                {
                    HttpResponseMessage httpResponseMessage = new HttpResponseMessage()
                    {
                        StatusCode = HttpStatusCode.OK,
                        Content = new StringContent("{\"APIRes\":" + JsonConvert.SerializeObject(apiResObj) + "}")
                    };

                    filterContext.Response = httpResponseMessage;
                    filterContext.Response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                }
                //apiResObj.ResCode = chkResCode;
                //if (string.IsNullOrEmpty(chkResCode) || !chkResCode.Equals("000"))
                //{
                //	switch (chkResCode)
                //	{
                //		case "500":
                //			apiResObj.ResMessage = "User Token不得為空";
                //			break;
                //		case "501":
                //			apiResObj.ResMessage = "Login UID不得為空";
                //			break;
                //		case "502":
                //			apiResObj.ResMessage = "User Token驗證失敗";
                //			break;
                //		case "503":
                //			apiResObj.ResMessage = "User Token與Operate UID不同";
                //			break;
                //		default:
                //			apiResObj.ResMessage = "User Token驗證發生未知錯誤";
                //			break;
                //	}


                //	//{"APIRes":{"ResCode":"100","ResMsg":"登入人員員工識別碼不得為空"}} 
                //	HttpResponseMessage httpResponseMessage = new HttpResponseMessage()
                //	{
                //		StatusCode = HttpStatusCode.OK,
                //		Content = new StringContent("{\"APIRes\":\"" + JsonConvert.SerializeObject(apiResObj) + "\"}")
                //	};

                //	filterContext.Response = httpResponseMessage;
                //	filterContext.Response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                //}
            }
        }
    }
}