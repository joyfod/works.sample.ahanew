﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Microsoft.Owin;
using Owin;

//[assembly: OwinStartupAttribute("GatorsConfig", typeof(GHL.API.Startup))]

namespace ECSystem.API
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}