﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.UserModel
{
    public class VerifyCodeModel
    {
        /// <summary>
        /// 驗證碼
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 啟動的URL
        /// </summary>
        public string ReturnUrl { get; set; }
    }
}
