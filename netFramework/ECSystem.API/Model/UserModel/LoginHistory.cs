﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.UserModel
{
    public class LoginHistory
    {
        public int MemberID { get; set; }
        public int LoginAreaID { get; set; }
        public string LoginIP { get; set; }
        public DateTime LoginTime { get; set; } = DateTime.Now;
        public string Platform { get; set; }
        public string ActionOS { get; set; }
        public string OSVersion { get; set; }
        public DateTime CreateDate { get; set; } = DateTime.Now;

    }
}
