﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.UserModel
{
    public class Commodity
    {
        public Guid ProductID { get; set; } = Guid.NewGuid();
        public string ProductName { get; set; }
        public decimal Qty { get; set; }
        public decimal Price { get; set; }
        public decimal Inventory { get; set; }
    }
}
