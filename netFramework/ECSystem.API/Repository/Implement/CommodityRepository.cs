﻿using Component.Helper;
using Component.Model;
using Model.UserModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Implement
{
    public class CommodityRepository
    {
        public bool InsterCommodity(Commodity model)
        {
            bool result = false;
            try
            {
                RedisHelper redisHelper = new RedisHelper();
                redisHelper.AddCache($"{model.ProductID.ToString("N")}", JsonConvert.SerializeObject(model), 3650000);
                result = true;
            }
            catch (Exception er)
            {
                // log
            }

            return result;
        }

        public List<Commodity> FindCommodity(Commodity model)
        {
            List<Commodity> result = new List<Commodity>();
            try
            {
                RedisHelper redisHelper = new RedisHelper();
                List<string> values = redisHelper.FindAllValues();
                List<Commodity> tmp = new List<Commodity>();

                if (values != null && values.Count > 0)
                {
                    tmp = JsonConvert.DeserializeObject<List<Commodity>>(JsonConvert.SerializeObject(values));
                }

                if (model != null)
                {
                    if (!string.IsNullOrEmpty(model.ProductName))
                    {
                        tmp = tmp.Where(x => x.ProductName.Contains(model.ProductName)).ToList();
                    }

                    if (model.Qty > 0)
                    {
                        tmp = tmp.Where(x => x.Qty >= model.Qty).ToList();
                    }

                    if (model.Price > 0)
                    {
                        tmp = tmp.Where(x => x.Price >= model.Price).ToList();
                    }

                    if (model.Inventory > 0)
                    {
                        tmp = tmp.Where(x => x.Inventory >= model.Inventory).ToList();
                    }
                }
                result.AddRange(tmp);
            }
            catch (Exception er)
            {
                // log
            }

            return result;
        }

        public bool UpdateCommodity(Commodity model)
        {
            bool result = false;
            try
            {
                RedisHelper redisHelper = new RedisHelper();
                redisHelper.SetCache(model.ProductID.ToString("N"), JsonConvert.SerializeObject(model), 3650000);
                // 可執行Signalr傳送也可以在signalr server 進行redis的subscript

                string listenChannel = "CommodityPublish";
                listenChannel = ConfigurationManager.AppSettings["PublishChannel"];
                redisHelper.Publish(listenChannel, model);
                result = true;
            }
            catch (Exception er)
            {
                // log
            }

            return result;
        }
    }
}
