﻿using Component.Helper;
using Component.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Implement
{
    public class MemberShipRepository
    {

        public bool ChangePwd(MemberShip model)
        {
            bool result = false;
            try
            {
                RedisHelper redisHelper = new RedisHelper();
                string checkData = redisHelper.GetCache(model.Account.ToUpper());

                if (!string.IsNullOrEmpty(checkData) && model != null)
                {
                    int liftTime = 36000000;
                    redisHelper.SetCache(model.Account.ToUpper(), JsonConvert.SerializeObject(model), liftTime);

                    result = true;
                }
            }
            catch (Exception er)
            {
                // write log
            }
            return result;
        }

        public MemberShip Login(string account, string pwd, string googleAuth = "", string fbAuth = "")
        {
            MemberShip result = new MemberShip();
            try
            {
                RedisHelper redisHelper = new RedisHelper();
                List<string> allRedisValue = redisHelper.FindAllValues();
                // 取出已認證的用戶
                List<MemberShip> allMember = JsonConvert.DeserializeObject<List<MemberShip>>(JsonConvert.SerializeObject(allRedisValue)).Where(x => x.IsEnable && x.IsValid).ToList();

                if (allMember != null && allMember.Count > 0)
                {
                    if (string.IsNullOrEmpty(googleAuth) && string.IsNullOrEmpty(fbAuth))
                    {
                        result = allMember.FirstOrDefault(x => x.Account.ToUpper().Equals(account.ToUpper()) && x.Pwd.ToUpper().Equals(pwd.SHA256Encrypt().ToUpper()));
                    }
                    else
                    {
                        result = allMember.FirstOrDefault(x => x.GoogleAuth.ToUpper().Equals(googleAuth.ToUpper()) || x.FaceBookAuth.ToUpper().Equals(fbAuth.ToUpper()));
                    }
                }
                else
                {
                    MersenneHelper mersenneHelper = new MersenneHelper();
                    int addressNo = mersenneHelper.MersenneRandom(1, 1000, 1, false)[0];
                    result = new MemberShip()
                    {
                        Account = account,
                        Pwd = pwd.SHA256Encrypt(),
                        NickName = account,
                        Address = $"No. {addressNo} Test RD. Test St.",
                        Phone = $"+995512324789",
                        IsEnable = true
                    };
                }
            }
            catch (Exception er)
            {
                // write log
            }

            return result;
        }

        public bool Logout(string userToken)
        {
            bool result = true;

            return result;
        }

        public bool Register(MemberShip model)
        {
            bool result = false;
            try
            {
                RedisHelper redisHelper = new RedisHelper();
                string checkData = redisHelper.GetCache(model.Account.ToUpper());

                if (string.IsNullOrEmpty(checkData))
                {
                    int liftTime = 36000000;
                    redisHelper.AddCache(model.Account.ToUpper(), JsonConvert.SerializeObject(model), liftTime);

                    result = true;
                }
            }
            catch (Exception er)
            {
                // write log
            }
            return result;
        }

        public bool ConfrimEmail(string publicKey, string code)
        {
            bool result = false;

            try
            {
                RedisHelper redisHelper = new RedisHelper();
                List<string> allRedisValue = redisHelper.FindAllValues();
                List<MemberShip> allMember = JsonConvert.DeserializeObject<List<MemberShip>>(JsonConvert.SerializeObject(allRedisValue));

                if (!string.IsNullOrEmpty(publicKey) && !string.IsNullOrEmpty(code))
                {
                    MemberShip model = allMember.FirstOrDefault(x => x.PublicKey.ToUpper().Equals(publicKey) && x.MailValidCode.ToUpper().Equals(code.ToUpper()));
                    if (model != null)
                    {
                        model.IsEnable = true;
                        model.IsValid = true;

                        int liftTime = 36000000;
                        redisHelper.SetCache(model.Account.ToUpper(), JsonConvert.SerializeObject(model), liftTime);
                        result = true;
                    }
                }
            }
            catch (Exception er)
            {
                // write log
            }

            return result;
        }
    }
}
