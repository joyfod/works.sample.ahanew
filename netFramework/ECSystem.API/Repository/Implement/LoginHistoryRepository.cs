﻿using Component.Helper;
using Model.UserModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Implement
{
    public class LoginHistoryRepository
    {

        public bool AddLoginHistory(LoginHistory loginHistory)
        {
            bool result = true;

            try
            {
                RedisHelper redisHelper = new RedisHelper();
                redisHelper.AddCache($"{DateTime.Now.ToString("yyyyMMddHHmmss")}_{loginHistory.MemberID}_", JsonConvert.SerializeObject(loginHistory), 365000);
            }
            catch (Exception er)
            {
                // write log
            }

            return result;
        }
    }
}
