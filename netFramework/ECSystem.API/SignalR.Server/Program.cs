﻿using Component.Helper;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Client;
using SignalR.Server.Model;
using SignalR.Server.Service;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SignalR.Server
{
    class Program
    {
        public static BindingList<ClientItem> clients = new BindingList<ClientItem>();
        private static string url = "localhost";
        private static string port = "8600";
        private static string signalServerURL = "";
        private static SignalRClientHelper pushOpenLotteryResult;
        private static HubConnection signalRConnection;


        static void Main(string[] args)
        {
            url = ConfigurationManager.AppSettings["ServerURL"];
            port = ConfigurationManager.AppSettings["ServerPort"];
            signalServerURL = ConfigurationManager.AppSettings["SignalServerURL"];

            IHubContext hubContext = GlobalHost.ConnectionManager.GetHubContext<SignalRHub>();

            GlobalHost.Configuration.ConnectionTimeout = TimeSpan.FromSeconds(120);
            GlobalHost.Configuration.DisconnectTimeout = TimeSpan.FromSeconds(30);

            SignalRServer server = new SignalRServer(url, port);
            server.Start();

            try
            {
                string subScriptChannel = !(string.IsNullOrEmpty(ConfigurationManager.AppSettings["SubScriptChannel"])) ? ConfigurationManager.AppSettings["SubScriptChannel"] : "CommoditySubScript";
                string connectStr = $"{ConfigurationManager.AppSettings["RedisServer"]}:{ConfigurationManager.AppSettings["RedisPort"]}";
                // if you have redis cluster (master & slave), you can use: master:6379,slave:6379
                var configuration = ConfigurationOptions.Parse(connectStr);
                //configuration.AllowAdmin = true;
                //configuration.Password = "123";

                // initial a connection
                ConnectionMultiplexer redis = ConnectionMultiplexer.Connect(configuration);

                redis.GetSubscriber().Subscribe(subScriptChannel, (channel, message) =>
                {
                    Console.WriteLine(message);
                    hubContext.Clients.All.addMessage("SERVER", message);
                });
            }
            catch (Exception er)
            {

            }

            // 可做循環檢查
            Thread threadSendRoom = new Thread(() =>
            {
                while (true)
                {
                    Thread.Sleep(100000);
                }
            });
            threadSendRoom.Start();
        }
    }
}
