﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SignalR.Server.Model
{
    class ClientItem : INotifyPropertyChanged
    {
        private string _name;

        public event PropertyChangedEventHandler PropertyChanged;

        public string RoomName { get; set; }
        public string ClientId { get; set; }
        public int MemberID { get; set; } = 0;
        public string UserName { get; set; } = "";
        public string NickName { get; set; } = "";
        public string ClientToken { get; set; }

        public string ClientUserName
        {
            get { return _name; }
            set
            {
                _name = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Name"));
            }
        }
    }
}
