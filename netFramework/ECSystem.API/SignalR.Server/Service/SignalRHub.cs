﻿using System;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using Microsoft.AspNet.SignalR;
using System.ComponentModel;
using System.Linq;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading;
using SignalR.Server.Model;

namespace SignalR.Server.Service
{
    public delegate void ClientConnectionEventHandler(string clientId);
    public delegate void ClientGroupEventHandler(string nowClientId, string roomName);

    public delegate void MessageReceivedEventHandler(string senderClientId, string message, string roomID = null);

    public class SignalRHub : Hub
    {
        static ConcurrentDictionary<string, string> _users = new ConcurrentDictionary<string, string>();
        private BindingList<ClientItem> clients = new BindingList<ClientItem>();

        public static event ClientConnectionEventHandler ClientConnected;
        public static event ClientConnectionEventHandler ClientDisconnected;

        public static event ClientGroupEventHandler ClientJoinedToRoom;
        public static event ClientGroupEventHandler ClientLeftRoom;

        public static event MessageReceivedEventHandler MessageReceived;

        public SignalRHub()
        {

        }

        public static void ClearState()
        {
            _users.Clear();
        }

        //Called when a client is connected
        public override Task OnConnected()
        {
            _users.TryAdd(Context.ConnectionId, Context.ConnectionId);

            clients.Add(new ClientItem() { ClientId = Context.ConnectionId, ClientUserName = Context.ConnectionId });
            ClientConnected?.Invoke(Context.ConnectionId);
            return base.OnConnected();
        }

        public override Task OnReconnected()
        {
            Console.WriteLine($"{Context.ConnectionId}-Reconnection");
            return base.OnReconnected();
        }

        //Called when a client is disconnected
        public override Task OnDisconnected(bool stopCalled)
        {
            string userName;
            _users.TryRemove(Context.ConnectionId, out userName);

            ClientDisconnected?.Invoke(Context.ConnectionId);

            return base.OnDisconnected(stopCalled);
        }

        #region Client Methods

        public void SetUserName(string userName, string memberID)
        {
            _users[Context.ConnectionId] = userName;

            int iMemberID = 0;
            int.TryParse(memberID, out iMemberID);

            int clientIndex = -1;
            if (Program.clients.ToList().Any(x => x.ClientId == Context.ConnectionId))
            {
                clientIndex = Program.clients.ToList().FindIndex(x => x.ClientId == Context.ConnectionId);
            }
        }

        public async Task JoinRoom(string roomName)
        {
            await Groups.Add(Context.ConnectionId, roomName);
        }

        public async Task LeaveRoom(string roomName)
        {
            await Groups.Remove(Context.ConnectionId, roomName);

            int clientIndex = Program.clients.ToList().FindIndex(x => x.ClientId == Context.ConnectionId);

            clients[clientIndex].RoomName = null;
        }

        public void Send(string msg, string roomID = null)
        {
            if (!string.IsNullOrEmpty(roomID))
            {
                var client = clients.Where(x => x.ClientId == Context.ConnectionId).FirstOrDefault();

                Clients.Group(roomID).addMessage(_users[Context.ConnectionId], msg);
            }
            else
            {
                Clients.All.addMessage(_users[Context.ConnectionId], msg);
            }

            MessageReceived?.Invoke(Context.ConnectionId, msg, roomID);
        }

        #endregion       
    }

}
