﻿using System;
using System.Collections.Generic;
using Owin;
using Microsoft.Owin.Cors;
using Microsoft.Owin.Hosting;
using Microsoft.AspNet.SignalR;
using System.ComponentModel;
using System.Threading;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR.Client;
using Microsoft.AspNet.SignalR.Hubs;
using Newtonsoft.Json;
using SignalR.Server.Model;
using StackExchange.Redis;
using System.Configuration;

namespace SignalR.Server.Service
{
    public class SignalRServer
    {
        private IDisposable _signalR;
        private BindingList<string> _groups = new BindingList<string>();
        private string url = "localhost";
        private string port = "8600";
        private static HubConnection signalRConnection;

        public SignalRServer(string _url, string _port)
        {
            url = _url;
            port = _port;

            //Register to static hub events
            SignalRHub.ClientConnected += SignalRHub_ClientConnected;
            SignalRHub.ClientDisconnected += SignalRHub_ClientDisconnected;
            SignalRHub.ClientJoinedToRoom += SignalRHub_ClientJoinedToRoom;
            SignalRHub.ClientLeftRoom += SignalRHub_ClientLeftRoom;
            SignalRHub.MessageReceived += SignalRHub_MessageReceived;
        }

        /// <summary>
        /// 連線
        /// </summary>
        /// <param name="clientId"></param>
        public void SignalRHub_ClientConnected(string clientId)
        {
            //Add client to our clients list
            Program.clients.Add(new ClientItem() { ClientId = clientId, ClientUserName = clientId });

            writeToLog($"Client connected:{clientId}");
        }

        /// <summary>
        /// 離線
        /// </summary>
        /// <param name="clientId"></param>
        public void SignalRHub_ClientDisconnected(string clientId)
        {
            //Remove client from the list
            var client = Program.clients.FirstOrDefault(x => x.ClientId == clientId);
            if (client != null)
                Program.clients.Remove(client);

            writeToLog($"Client disconnected:{clientId}");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="newName"></param>
        public void SignalRHub_ClientNameChanged(string clientId, string newName)
        {
            //Update the client's name if it exists
            var client = Program.clients.FirstOrDefault(x => x.ClientId == clientId);
            if (client != null)
                client.ClientUserName = newName;

            writeToLog($"Client name changed. Id:{clientId}, Name:{newName}");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nowClientId"></param>
        /// <param name="roomSetID"></param>
        /// <param name="lotteryTypeID"></param>
        /// <param name="roomID"></param>
        /// <param name="seatID"></param>
        /// <param name="currentPeriod"></param>
        /// <param name="lastClientId"></param>
        public void SignalRHub_ClientJoinedToRoom(string nowClientId, string roomName)
        {
            //Only add the groups name to our groups list
            var group = _groups.FirstOrDefault(x => x == roomName);
            if (group == null)
                _groups.Add(roomName);

            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine($"Service -- nowClientId:::{nowClientId}--roomName:::{roomName}");

            int clientIndex = Program.clients.ToList().FindIndex(x => x.ClientId == nowClientId);
            Console.WriteLine($"取得 clientIndex {clientIndex} ");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nowClientId"></param>
        /// <param name="roomSetID"></param>
        /// <param name="lotteryTypeID"></param>
        /// <param name="roomID"></param>
        /// <param name="seatID"></param>
        /// <param name="currentPeriod"></param>
        /// <param name="lastClientId"></param>
        public void SignalRHub_ClientLeftRoom(string nowClientId, string roomName)
        {
            foreach (var x in Program.clients)
            {
                if (x.ClientId == nowClientId)
                {
                    // 將位置清空
                    x.RoomName = null;
                }
            }
            writeToLog($"Client left group. Id:{nowClientId}, Group:{roomName}");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="senderClientId"></param>
        /// <param name="message"></param>
        /// <param name="roomID"></param>
        public void SignalRHub_MessageReceived(string senderClientId, string message, string roomName = null)
        {
            //One of the clients sent a message, log it
            //string clientName = _clients.FirstOrDefault(x => x.ClientId == senderClientId)?.ClientUserName;
            ClientItem client = Program.clients.FirstOrDefault(x => x.ClientId == senderClientId);
            string clientName = client.ClientUserName;
            string clientGroup = client.RoomName;

            writeToLog($"{clientName}-{clientGroup}:{message}");
        }

        /// <summary>
        /// 
        /// </summary>
        public void Start()
        {
            try
            {
                //Start SignalR server with the give URL address
                //Final server address will be "URL/signalr"
                //Startup.Configuration is called automatically
                _signalR = WebApp.Start<Startup>($"http://{url}:{port}");

                writeToLog($"Server started at: http://{url}:{port}");
                //Console.WriteLine("server running...");
            }
            catch (Exception ex)
            {
                writeToLog($"Error:{ex.Message} {Environment.NewLine}");
                //Console.WriteLine($"Error:{ex.Message} {Environment.NewLine}");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="log"></param>
        private void writeToLog(string log)
        {
            Console.WriteLine(log + Environment.NewLine);
        }
    }
}
