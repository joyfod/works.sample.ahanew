﻿using Component.Base;
using Component.Helper;
using Component.Model;
using Model.UserModel;
using Newtonsoft.Json;
using Repository.Implement;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Implement
{
    public class CommodityService
    {
        /// <summary>
        /// 上架
        /// </summary>
        /// <param name="productName"></param>
        /// <param name="qty"></param>
        /// <param name="price"></param>
        /// <param name="inventory"></param>
        /// <returns></returns>
        public Hashtable InsterCommodity(string productName, int qty, decimal price, decimal inventory)
        {
            Hashtable hashtable = new Hashtable();
            APIResObj apiResObj = new APIResObj();

            try
            {
                CommodityRepository commodityRepository = new CommodityRepository();
                Commodity commodity = new Commodity()
                {
                    ProductName = productName,
                    Qty = qty,
                    Price = price,
                    Inventory = inventory
                };
                commodityRepository.InsterCommodity(commodity);
            }
            catch (Exception er)
            {
                apiResObj.ResCode = "400";
                apiResObj.ResMessage = er.Message;
            }
            finally
            {
                hashtable.Add("APIRes", apiResObj);
            }

            return hashtable;
        }

        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="productName"></param>
        /// <param name="qty"></param>
        /// <param name="price"></param>
        /// <param name="inventory"></param>
        /// <returns></returns>
        public Hashtable FindCommodity(string productName, int qty, decimal price, decimal inventory)
        {
            Hashtable hashtable = new Hashtable();
            APIResObj apiResObj = new APIResObj();

            try
            {
                CommodityRepository commodityRepository = new CommodityRepository();
                Commodity commodity = new Commodity()
                {
                    ProductName = productName,
                    Qty = qty,
                    Price = price,
                    Inventory = inventory
                };
                List<Commodity> commodities = commodityRepository.FindCommodity(commodity);
                hashtable.Add("Rows", commodities);
            }
            catch (Exception er)
            {
                apiResObj.ResCode = "400";
                apiResObj.ResMessage = er.Message;
            }
            finally
            {
                hashtable.Add("APIRes", apiResObj);
            }

            return hashtable;
        }


        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="productName"></param>
        /// <param name="qty"></param>
        /// <param name="price"></param>
        /// <param name="inventory"></param>
        /// <returns></returns>
        public Hashtable UpdateCommodity(Guid productId, string productName, int qty, decimal price, decimal inventory)
        {
            Hashtable hashtable = new Hashtable();
            APIResObj apiResObj = new APIResObj();

            try
            {
                CommodityRepository commodityRepository = new CommodityRepository();
                Commodity commodity = new Commodity()
                {
                    ProductID = productId,
                    ProductName = productName,
                    Qty = qty,
                    Price = price,
                    Inventory = inventory
                };
                commodityRepository.UpdateCommodity(commodity);
            }
            catch (Exception er)
            {
                apiResObj.ResCode = "400";
                apiResObj.ResMessage = er.Message;
            }
            finally
            {
                hashtable.Add("APIRes", apiResObj);
            }

            return hashtable;
        }

    }
}
