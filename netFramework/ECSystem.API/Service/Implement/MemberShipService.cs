﻿using Component.Base;
using Component.Helper;
using Component.Model;
using Model.UserModel;
using Newtonsoft.Json;
using Repository.Implement;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.Implement
{
    public class MemberShipService
    {
        private StackTrace st = new StackTrace();

        #region 登入、登出
        /// <summary>
        /// 登入
        /// </summary>
        /// <param name="memberUser"></param>
        /// <param name="pwd"></param>
        /// <param name="ip"></param>
        /// <param name="plateForm"></param>
        /// <param name="browerInfo"></param>
        /// <param name="userAgent"></param>
        /// <param name="validCode"></param>
        /// <param name="inputCode"></param>
        /// <param name="isMandatory"></param>
        /// <returns></returns>
        public Hashtable Login(string memberUser, string pwd, string ip, string plateForm, string browerInfo,
                               string userAgent, string validCode = null, string inputCode = null,
                               bool isMandatory = false, string googleAuth = "", string fbAuth = "")
        {
            Hashtable hashtable = new Hashtable();
            APIResObj apiResObj = new APIResObj();
            MemberShip memberShip = new MemberShip();

            try
            {
                MemberShipRepository memberShiptRepository = new MemberShipRepository();
                LoginHistoryRepository loginHistoryRepository = new LoginHistoryRepository();

                if (!string.IsNullOrEmpty(inputCode))
                {
                    string userInput = HelperUtil.UserProcessRequestEncry(inputCode);

                    if (!validCode.Equals(userInput))
                    {
                        apiResObj.ResCode = "200";
                        apiResObj.ResMessage = "Login Fail，Please Confirm Verification Code.";
                        return hashtable;
                    }
                }

                if (string.IsNullOrEmpty(googleAuth) && string.IsNullOrEmpty(fbAuth))
                {
                    ValidatorPwd validatorPwd = HelperUtil.RegexPwd(pwd);
                    if (!validatorPwd.CheckResult)
                    {
                        apiResObj.ResCode = "200";
                        apiResObj.ResMessage = $"Login Fail，{validatorPwd.ValidatorMsg}";
                        return hashtable;
                    }
                }

                memberShip = memberShiptRepository.Login(memberUser, pwd);
                if (memberShip == null || memberShip.Pwd != pwd.SHA256Encrypt())
                {
                    apiResObj.ResCode = "200";
                    apiResObj.ResMessage = "Login Fail，Username or Password was wrong.";
                    return hashtable;
                }

                #region 強制登入流程
                try
                {
                    if (!memberShip.IsEnable)
                    {
                        apiResObj.ResCode = "201";
                        apiResObj.ResMessage = "Your Account already suspended, Please contact CS for further infomation.";
                        return hashtable;
                    }

                    if (memberShip.IsLock)
                    {
                        apiResObj.ResCode = "201";
                        apiResObj.ResMessage = "Login Fail，Your Account is Locked.";
                        return hashtable;
                    }

                    //登入成功記錄至LoginHistory
                    LoginHistory loginHistory = new LoginHistory();
                    loginHistory.MemberID = memberShip.MemberID;
                    loginHistory.LoginAreaID = 1;
                    loginHistory.LoginIP = ip;
                    loginHistory.LoginTime = DateTime.Now;
                    loginHistory.Platform = plateForm;
                    loginHistory.ActionOS = browerInfo;
                    loginHistory.OSVersion = userAgent;
                    loginHistory.CreateDate = DateTime.Now;
                    loginHistoryRepository.AddLoginHistory(loginHistory);

                    apiResObj.ResCode = "000";
                    apiResObj.ResMessage = $"Login Success， Welcome {memberShip.Account}！";

                    //memberShip.UserName = HelperUtil.MaskAcccount(memberShip.UserName);
                    memberShip.PublicKey = $"{memberShip.Account}_{memberShip.Pwd}";
                    memberShip.Pwd = null;

                    int testInt = 0;
                    int lifeTimeSec = 180;
                    lifeTimeSec = int.TryParse(ConfigurationManager.AppSettings["PlayTokenOTP"], out testInt) ? testInt : lifeTimeSec;

                    string playTokenOTP = GenRegisterOTP(memberShip.PublicKey, lifeTimeSec, false);

                    string userToken = SecurityKeyHelper.Encrypt(JsonConvert.SerializeObject(memberShip), true);
                    // 寫入redis的登入
                    //RedisHelper redisHelper = new RedisHelper();
                    //redisHelper.AddCache(userToken, JsonConvert.SerializeObject(memberShip), lifeTimeSec);

                    hashtable.Add("Rows", memberShip);
                    hashtable.Add("AuthToken", userToken);
                    hashtable.Add("UserName", memberShip.PublicKey);
                    hashtable.Add("PlayToken", playTokenOTP);
                }
                catch (Exception er)
                {
                    apiResObj.ResCode = "200";
                    apiResObj.ResMessage = "Username or Password was wrong.";
                }
                #endregion
            }
            catch (Exception er)
            {
                apiResObj.ResCode = "400";
                apiResObj.ResMessage = er.Message;
            }
            finally
            {
                hashtable.Add("APIRes", apiResObj);
            }

            return hashtable;
        }

        /// <summary>
        /// 登出
        /// </summary>
        /// <param name="userToken"></param>
        /// <returns></returns>
        public Hashtable Logout(string userToken)
        {
            Hashtable hashtable = new Hashtable();
            APIResObj apiResObj = new APIResObj();
            bool result = new bool();

            try
            {
                MemberShipRepository memberShiptRepository = new MemberShipRepository();

                // 寫入redis的登入
                RedisHelper redisHelper = new RedisHelper();
                redisHelper.SetCache(userToken, null, 0);

                result = memberShiptRepository.Logout(userToken);
            }
            catch (Exception er)
            {
                apiResObj.ResCode = "400";
                apiResObj.ResMessage = er.Message;
            }
            finally
            {
                hashtable.Add("APIRes", apiResObj);
                hashtable.Add("Rows", result);
            }

            return hashtable;
        }
        #endregion

        /// <summary>
        /// 註冊
        /// </summary>
        /// <param name="account"></param>
        /// <param name="pwd"></param>
        /// <param name="nickName"></param>
        /// <param name="address"></param>
        /// <param name="phone"></param>
        /// <param name="email"></param>
        /// <returns></returns>
        public Hashtable Register(string account = "", string pwd = "", string nickName = "", string address = "",
                                  string phone = "", string email = "", string googleAuth = "", string fbAuth = "")
        {
            Hashtable hashtable = new Hashtable();
            APIResObj apiResObj = new APIResObj();
            bool result = new bool();

            try
            {
                MemberShipRepository memberShiptRepository = new MemberShipRepository();

                ValidatorPwd validatorPwd = HelperUtil.RegexPwd(pwd);
                if (!validatorPwd.CheckResult)
                {
                    apiResObj.ResCode = "200";
                    apiResObj.ResMessage = $"Register Fail，{validatorPwd.ValidatorMsg}";
                    return hashtable;
                }

                MemberShip model = new MemberShip()
                {
                    Account = account,
                    Pwd = pwd.SHA256Encrypt(),
                    NickName = nickName,
                    Address = address,
                    Phone = phone,
                    Email = email,
                    IsEnable = false,
                    IsLock = false,
                    IsValid = false,
                    PublicKey = $"{account}_{pwd}",
                    GoogleAuth = googleAuth,
                    FaceBookAuth = fbAuth
                };

                if (!string.IsNullOrEmpty(googleAuth) || !string.IsNullOrEmpty(fbAuth))
                {
                    model.IsValid = true;
                    model.IsEnable = true;
                }

                result = memberShiptRepository.Register(model);
                if (!result)
                {
                    apiResObj.ResCode = "201";
                    apiResObj.ResMessage = $"{account}:重複註冊";
                    hashtable.Add("Rows", result);
                    return hashtable;
                }
                // 重發認證信
                else
                {
                    if (!model.IsValid)
                    {
                        string validCodeUrl = ConfigurationManager.AppSettings["ValidCodeUrl"];
                        //寄出信件
                        #region sendMail
                        MailUtil sendMail = new MailUtil();
                        sendMail.mailAccount = ConfigurationManager.AppSettings["MAIL_ACCOUNT"];
                        sendMail.mailPwd = ConfigurationManager.AppSettings["MAIL_PASSWORD"];
                        sendMail.smtpPort = Convert.ToInt32(ConfigurationManager.AppSettings["MAIL_PORT"]);
                        sendMail.smtpServer = ConfigurationManager.AppSettings["MAIL_HOST"];
                        sendMail.SendMailByGmail(new List<string>() { model.Email }, "From System Mail", $"Your password:{model.Pwd} \r\n Validator URL:{validCodeUrl}/{model.PublicKey}/{model.MailValidCode}");
                        #endregion

                        result = true;
                        apiResObj.ResCode = "000";
                        apiResObj.ResMessage = "The vaildator send to your E-mail！";
                    }
                }
            }
            catch (Exception er)
            {
                apiResObj.ResCode = "400";
                apiResObj.ResMessage = er.Message;
            }
            finally
            {
                hashtable.Add("APIRes", apiResObj);
                hashtable.Add("Rows", result);
            }

            return hashtable;
        }

        /// <summary>
        /// 修改密碼
        /// </summary>
        /// <param name="userToken"></param>
        /// <param name="oldPwd"></param>
        /// <param name="newPwd"></param>
        /// <returns></returns>
        public Hashtable ChangePwd(string userToken, string oldPwd, string newPwd)
        {
            Hashtable hashtable = new Hashtable();
            APIResObj apiResObj = new APIResObj();
            bool result = new bool();
            MemberShip member = new MemberShip();

            try
            {
                string token = SecurityKeyHelper.Decrypt(userToken, true);
                member = JsonConvert.DeserializeObject<MemberShip>(token);

                MemberShipRepository memberShiptRepository = new MemberShipRepository();
                result = memberShiptRepository.ChangePwd(member);
                memberShiptRepository.Logout(userToken);

                if (!result)
                {

                }
            }
            catch (Exception er)
            {
                apiResObj.ResCode = "400";
                apiResObj.ResMessage = er.Message;
            }
            finally
            {
                hashtable.Add("APIRes", apiResObj);
                hashtable.Add("Rows", result);
            }

            return hashtable;
        }

        /// <summary>
        /// 信箱啟用
        /// </summary>
        /// <param name="userId">用戶ID</param>
        /// <param name="code">信箱驗證碼</param>
        /// <returns></returns>
        public Hashtable ConfirmEmail(string publicKey, string code)
        {
            Hashtable hashtable = new Hashtable();
            APIResObj apiResObj = new APIResObj();
            bool result = new bool();
            string callBackURL = "";

            try
            {
                MemberShipRepository memberShiptRepository = new MemberShipRepository();

                result = memberShiptRepository.ConfrimEmail(publicKey, code);

                if (result)
                {
                    callBackURL = ConfigurationManager.AppSettings["CallBackURL"];
                }
            }
            catch (Exception er)
            {
                apiResObj.ResCode = "400";
                apiResObj.ResMessage = er.Message;
            }
            finally
            {
                hashtable.Add("APIRes", apiResObj);
                hashtable.Add("Rows", result);
            }

            return hashtable;
        }

        private string GenRegisterOTP(string values, int lifeTimeSec, bool isMinute = true)
        {
            string result = OTPHelper.GenOTP();

            string value = SecurityKeyHelper.Encrypt($"{DateTime.Now.ToString("yyyyMMddHHmmsss")}", true);

            RedisHelper playRedisHelper;
            string redisServer = ConfigurationManager.AppSettings["RedisServer"];
            string redisPort = ConfigurationManager.AppSettings["RedisPort"];
            string redisDB = ConfigurationManager.AppSettings["RegisterRedisDB"];

            playRedisHelper = new RedisHelper(sDB: redisDB, sRedisServer: redisServer, sRedisPort: redisPort, syncTimeoutSec: lifeTimeSec);

            playRedisHelper.SetCache(result, value, lifeTimeSec, isMinute);

            return result;
        }
    }
}
